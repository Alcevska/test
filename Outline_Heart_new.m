%Segment Grayscale
%Trying to see if I can segment the outline with the different channels of
%RGB with the grayscale images. 

close all;
%keyboard
orgImage = dicomread('FC9AGO82');  
figure, imshow(orgImage), title('original image');
figure, imagesc(orgImage), title('original image imagesc');

[a b c]=size(orgImage);
RedImage = orgImage(:,:,1);
GreenImage = orgImage(:,:,2);
BlueImage = orgImage(:,:,3);

%The value difference between RGB, The difference should be high between
%the RGB because then we can set the pixels that does not fulfill this case
%as grayscale pixels. Pixels that appears as colors have a large difference
%between red, green and blue values.
diff_of_color=25;

GrayImage=zeros(a,b,3,'uint8');

for i=1:1:b
    for k=1:1:a
        maxcolor=max(orgImage(k,i,:));
        if (((maxcolor-RedImage(k,i)) < diff_of_color) & ((maxcolor-BlueImage(k,i)) < diff_of_color) & ((maxcolor-GreenImage(k,i)) < diff_of_color));
            GrayImage(k,i,:)=0;
        else
            GrayImage(k,i,:)=orgImage(k,i,:);
        end
    end
end

figure(3), imshow(GrayImage), ('Removels of pixel colors');

NewGrayImage=GrayImage(48:380,222:500,:);                                  %Cut the image to a different size.
NewGrayImage=rgb2gray(NewGrayImage);                                       %Changed the image to grayscale.
figure, imshow(NewGrayImage), title('NewGrayImage');                       %Plotting the result
[Ix,Iy]=size(NewGrayImage);                                                %Size of the new Image

[column,row]=find(NewGrayImage);                                           %Finds all the values in the image that are not zero.
hold on 
plot(row, column,'-');

contour=boundary(row,column);
figure, imshow(NewGrayImage), title('Method boundary');
hold on, plot(row(contour),column(contour),'LineWidth',2);

%This part is for the straight line in the bottom of the heart Ventricle.
LineImage=edge(NewGrayImage,'canny');
[H,theta,rho]=hough(LineImage);
figure, imshow(LineImage), hold on 
P = houghpeaks(H,5,'threshold',ceil(0.3*max(H(:))));

lines = houghlines(LineImage,theta,rho,P,'FillGap',5,'MinLength',7);
x = theta(P(:,2));
y = rho(P(:,1));
plot(x,y,'s','color','black');

max_len = 0;
for k = 1:length(lines)
    xy = [lines(k).point1; lines(k).point2];
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
    
    % Plot beginnings and ends of lines
    plot(xy(1,1),xy(1,2),'x','LineWidth',2,'Color','yellow');
    plot(xy(2,1),xy(2,2),'x','LineWidth',2,'Color','red');
    
    % Determine the endpoints of the longest line segment
    len = norm(lines(k).point1 - lines(k).point2);
    if (len > max_len)
        max_len = len;
        xy_long = xy;
    end
end

Segment=poly2mask(row(contour),column(contour),Ix,Iy);
figure, imshow(Segment), title('Segment with poly2mask');                                                %Segment of the Heart Ventricle.

            

