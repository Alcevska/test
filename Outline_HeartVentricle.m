%Segment Grayscale
%Trying to see if I can segment the outline with the different channels of
%RGB with the grayscale images. 

close all;
%keyboard
orgImage = dicomread('FC9AGO82');  
figure(1), imshow(orgImage), title('original image');
figure(2), imagesc(orgImage), title('original image imagesc');

[a b c]=size(orgImage);
RedImage = orgImage(:,:,1);
GreenImage = orgImage(:,:,2);
BlueImage = orgImage(:,:,3);

%The value difference between RGB, The difference should be high between
%the RGB because then we can set the pixels that does not fulfill this case
%as grayscale pixels. Pixels that appears as colors have a large difference
%between red, green and blue values.
diff_of_color=25;

GrayImage=zeros(a,b,3,'uint8');

for i=1:1:b
    for k=1:1:a
        maxcolor=max(orgImage(k,i,:));
        if (((maxcolor-RedImage(k,i)) < diff_of_color) & ((maxcolor-BlueImage(k,i)) < diff_of_color) & ((maxcolor-GreenImage(k,i)) < diff_of_color));
            GrayImage(k,i,:)=0;
        else
            GrayImage(k,i,:)=orgImage(k,i,:);
        end
    end
end

figure(3), imshow(GrayImage), title('imshow GrayImage');
figure(4), imagesc(GrayImage), title('imagesc GrayImage');

NewGrayImage=GrayImage(48:380,222:500,:);                                  %Cut the image to a different size.
NewGrayImage=rgb2gray(NewGrayImage);                                       %Changed the image to grayscale.
figure(5), imshow(NewGrayImage), title('NewGrayImage');                    %Plotting the result
[Ix,Iy]=size(NewGrayImage);                                                %Size of the new Image

[column,row]=find(NewGrayImage);                                           %Finds all the values in the image that are not zero.
hold on 
plot(row, column,'-');

contour=boundary(row,column);
figure(6), imshow(NewGrayImage), title('Method boundary');
hold on, plot(row(contour),column(contour),'LineWidth',2);


Segment=poly2mask(row(contour),column(contour),Ix,Iy);
figure(7), imshow(Segment), title('Segment with poly2mask');                                                %Segment of the Heart Ventricle.

            

