%Segment with the help of pixel of color and remove Grayscale pixels

path='/Users/Alcevska/Documents/MATLAB/Test_Plot_4D/data_160203_nr2/G23EAIP4';

close all;
%keyboard
%orgImage = dicomread('FC9AGO82');  
%orgImage = dicomread('G1SATHG2'); 
orgImage = dicomread(path);
figure(1), imshow(orgImage), title('original image');
%figure(2), imagesc(orgImage), title('original image imagesc');

[a b c]=size(orgImage);
RedImage = orgImage(:,:,1);
GreenImage = orgImage(:,:,2);
BlueImage = orgImage(:,:,3);

%The value difference between RGB, The difference should be high between
%the RGB because then we can set the pixels that does not fulfill this case
%as grayscale pixels. Pixels that appears as colors have a large difference
%between red, green and blue values.
diff_of_color=20; %Blue/Purple/Green Outline %25

GrayImage=zeros(a,b,3,'uint8');

%Outline Blue or Green 
for i=1:1:b
    for k=1:1:a
        maxcolor=max(orgImage(k,i,:));
        if (((maxcolor-RedImage(k,i)) < diff_of_color) & ((maxcolor-BlueImage(k,i)) < diff_of_color) & ((maxcolor-GreenImage(k,i)) < diff_of_color));
            GrayImage(k,i,:)=0;
        else
            GrayImage(k,i,:)=orgImage(k,i,:);
        end
    end
end

% When Outline is Blue/Purple, If the yellow numbers are too close to the 
% outline, they will be considered as the outline = wrong segment. 
% for i=1:1:b
%     for k=1:1:a
%         if ((BlueImage(k,i)) < 135 | (RedImage(k,i) > 155));
%             GrayImage(k,i,:)=0;
%         else
%             GrayImage(k,i,:)=GrayImage(k,i,:);
%         end
%     end
% end

%When Outline is Greenish.
for i=1:1:b
    for k=1:1:a
        if ( ((GreenImage(k,i)-RedImage(k,i)) < 10) );
            GrayImage(k,i,:)=0;
        else
            GrayImage(k,i,:)=GrayImage(k,i,:);
        end
    end
end

figure(3), imshow(GrayImage), title('imshow GrayImage');
%figure(4), imagesc(GrayImage), title('imagesc GrayImage');

Row_start=45;
Row_end=375;
Column_start=180;
Column_end=500;

NewGrayImage=GrayImage(Row_start:Row_end,Column_start:Column_end,:);       %Cut the image to a different size.
NewGrayImage=rgb2gray(NewGrayImage);                                       %Changed the image to grayscale.

Nr_row=length(Row_start:Row_end);
Nr_column=length(Column_start:Column_end);
Segment=zeros(Nr_row,Nr_column);

%use this when you have issues with the boundary
NewGrayImage=double(NewGrayImage);

%figure(5), imshow(NewGrayImage), title('NewGrayImage');                   %Plotting the result
[Ix,Iy]=size(NewGrayImage);                                                %Size of the new Image

[row,column]=find(NewGrayImage);                                           %Finds all the values in the image that are not zero.
contour=boundary(row,column,1);

figure(6), imshow(NewGrayImage), title('Method boundary');
hold on, plot(column(contour),row(contour),'LineWidth',2);

for rad=1:1:Iy
    for kolumn=1:1:Ix
        Segment=inpolygon(rad,kolumn,row,column);
    end
end

figure(7), imshow(Segment), title('Segment with poly2mask');               %Segment of the Heart Ventricle.

zero_image=zeros(a,b);
zero_image(Row_start:Row_end,Column_start:Column_end)=Segment;
%zero_image(Row_start:Row_end,1)=Segment;

figure(8),
Original=orgImage;
imshow(Original), title('Original');
segmented=zero_image;
hold on;
S_image=imshow(segmented);
set(S_image,'AlphaData',0.4);

figure(9)
Original=GrayImage;
imshow(Original), title('Original');
segmented=zero_image;
hold on;
S_image=imshow(segmented);
set(S_image,'AlphaData',0.4);
