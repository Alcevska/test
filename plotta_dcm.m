data_path = '/Users/Alcevska/Documents/MATLAB/Test_Plot_4D'; % change to your location


addpath(genpath(data_path));
files = dir(fullfile(data_path, 'GEMS_IMG', '2015_MAR', '24', 'LJ162100')); 
files = files(3:end);

for i = 1:length(files)
   
    dcm_info = dicominfo(files(i).name);
    dcm_image = dicomread(files(i).name);
    
    fprintf('i = %d. Nbr of dims: %d, Patient id: %s, Instance number: %d.\n',...
        i, ndims(dcm_image), dcm_info.PatientID, dcm_info.InstanceNumber);
    
    for j = 1:size(dcm_image, 4)
       
       imagesc(squeeze(dcm_image(:, :, :, j)));
       hold on
       title('Press space for next slide.')
       
       pause
    end
    
    fprintf('End of images, press ''dbcont'' to continue (quit with dbquit)\n');
    keyboard
end