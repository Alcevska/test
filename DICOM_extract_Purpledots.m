%DICOM Extract Purpledots
close all;
clear all;
clc

orgImage = dicomread('FC9AGO82');                                          %las in bildmatrisen en 2D-bild har 3 lager som innehaller RGB, dessa har samma varden i en graskalebild. en bildsekvens har ytterligare en dimension pa bilden - ett antal 2D-bilder                                                 
figure(1)
imshow(orgImage), title('orgImage')
newImage=orgImage(:,:,3);                                                  %Blue channel of the image

figure(2)
imshow(newImage), title('newImage')
[a,b]=size(newImage);
Coordinfo=zeros(a,b);
n=0;

for i=1:1:(b-1)
    for k=1:1:a
        if (newImage(k,i)<220 && newImage(k,i)>170)
          Coordinfo(k,i)=1;
        else
          Coordinfo(k,i)=0;
        end
    end
end

figure(3),
imshow(Coordinfo)

