%DICOM reads IMAGE

% den har programsnutten laser in en dicom-bild de bortkommaterade raderna
% anvandes for att klippa ut nagra bilder ur orginalsekvensen for att fa
% ner filstorleken for att kunna maila.
close all                               % st?ng alla ?ppna figures
clear all                               % ta bort alla ?ppna variabler
clc

%[fileName,pathName,filterIndex] = uigetfile('*.*');
[fileName] = uigetfile('*.*');                                              % oppna en dialogruta, lat anvandaren valja aktuell dicom-fil
myAbsFileName = [fileName];                                                 % lagg ihop sokvag och filnamn
info = dicominfo(myAbsFileName);                                            % i info ligger alla taggar som hor till aktuell en dicom-fil
orgImage = dicomread(info);                                                 % las in bildmatrisen en 2D-bild har 3 lager som innehaller RGB, dessa har samma varden i en graskalebild. en bildsekvens har ytterligare en dimension pa bilden - ett antal 2D-bilder                                                 
imshow(orgImage), title('orgImage')


%% Which files can be open with DICOM? x=NO, o=YES
%FC9AFU80 x
%FC9AGO82 o (OK FIGUR)
%FC9AGTO4 x
%FC9AHS06 o
%FC9B1G80 x
%FC9B1H02 x
%FC9B1HG4 x
%FC9B1I06 x
%FC9B1I88 x
%FC9B1IOA x
%FC9B1J0C x
%FC9B1J8E x
%FC9B1JOG x
%FC9B1K8I x
%FC9B1KGK o
%FC9B1KGM x
%FC9B1KOO o
%FC9B1KOQ x
%FC9B1L8S x
%FC9B1LOU x
%FC9B1M90 x
%FC9B1MP2 x
%FC9B1N94 o
%FC9B1N96 o
%FC9B1NH8 o
%FC9B1NPA o
%FC9B1NPC x
%FC9B1O1E x
%FC9B1OHG o
%FC9B1OPI o
%FC9B1OPK x
%FC9B1P9M x
%FC9B1PPO x
%FC9B1Q1Q o 
%FC9B1Q1S x
%FC9B1QHU o
%FC9B1QI0 x
%FC9B1RA2 o
%FC9B1RA4 x
%FC9B1RQ6 o
%FC9B1RQ8 x
%FC9B1S2A x
%FC9B1SAC x
%FC9B1SQE o
%FC9B1SQG x
%FC9B1T2I o
%FC9B1T2K x
%FC9B1TAM x
%FC9B1TQO x
%FC9B1U2Q x
%FC9B1UIS x
%FC9B1UQU x
%FC9B20B0 o
%FC9B20J2 o 
%FC9B20J4 x
%FC9B20J6 o 
%FC9B20R8 x
%FC9B20RA o
%FC9B20RC o
%FC9B20RE o
%FC9B213G o (OK FIGUR)
%FC9B213I o (OK FIGUR)